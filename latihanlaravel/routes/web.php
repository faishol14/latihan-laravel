<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@submit');

Route::get('/data-table', function() {
    return view('table.data-table');
});

//CRUD cast
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@player');
Route::get('/cast', 'CastController@index');
Route::get('/show/{cast_id}', 'CastController@show');
Route::get('/show/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');

Route::middleware('auth')->group(function () {
//CRUD Genre
Route::get('/genre/create', 'GenreController@create');
Route::post('/genre', 'GenreController@sample');
Route::get('/genre', 'GenreController@index');
Route::get('/genre/{genre_id}', 'GenreController@show');
Route::get('/genre/{genre_id}/edit', 'GenreController@edit');
Route::put('/genre/{genre_id}', 'GenreController@update');
Route::delete('/genre/{genre_id}', 'GenreController@destroy');
});


//CRUD Film
route::resource('film', 'FilmController');
Auth::routes();


