@extends('layout.master')

@section('Data-Tables')
Daftar Genre
@endsection
 
@section('content')

<a href="/genre/create" class="btn btn-primary mb-3">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($genre as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>
                            
                            <form action="/genre/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/genre/{{$value->id}}" class="btn btn-info">Detail</a>
                                <a href="/genre/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                <input type="submit" onclick="return confirm('Apakah kamu yakin?')" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>


@endsection