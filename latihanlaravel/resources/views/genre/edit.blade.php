@extends('layout.master')

@section('Data-Tables')
Edit Genre Film {{$genre->nama}}
@endsection
 
@section('content')

<form action="/genre/{{$genre->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" value="{{$genre->nama}}" name="nama" placeholder="Masukkan Title">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>

@endsection