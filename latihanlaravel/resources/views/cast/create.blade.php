@extends('layout.master')

@section('Data-Tables')
Cast Player
@endsection
 
@section('content')

        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Title">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>umur</label>
                <input type="text" class="form-control" name="umur" placeholder="Masukkan Umur">
                <h6 class="text-danger mt-1" ><i>Tulis hanya angka, Contoh : 19.<i><h6>
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>bio</label>
                <textarea name="bio" class='form-control' cols="30" rows="10"></textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>

@endsection