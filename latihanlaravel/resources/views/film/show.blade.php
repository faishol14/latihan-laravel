@extends('layout.master')

@section('Data-Tables')
Detail Film id ke {{$film->id}}
@endsection


@section('content')

<img src="{{asset('images/'.$film->poster)}}" alt="">
<h4 class="text-primary"><b>{{$film->judul}}</b></h4>
<p>{{$film->ringkasan}}</p>

<p class="text-secondary">{{$film->created_at}}</p>

<a href="/film" class="btn btn-primary">kembali</a>
@endsection