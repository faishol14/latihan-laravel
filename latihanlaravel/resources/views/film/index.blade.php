@extends('layout.master')

@section('Data-Tables')
List Film
@endsection
 
@section('content')

@auth
  
<a href="/film/create" class="btn btn-primary mb-2">Tambah Film</a>
@endauth

<div class="row">
    @forelse ($film as $item)
        <div class="col-4">
            <div class="card" style="width: 18rem;">
                <img src="{{asset('images/'.$item->poster)}}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h3><b>{{$item->judul}}</b></h3>
                  <p class="card-text">{{ Str::limit($item->ringkasan, 116) }}</p>
                  @auth
                  <form action="/film/{{$item->id}}" method="POST">
                    @csrf
                    @method('Delete')
                    <a href="/film/{{$item->id}}" class="btn btn-primary btn-sm">DETAIL</a>
                    <a href="/film/{{$item->id}}/edit" class="btn btn-primary btn-sm">EDIT</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                  </form>  
                  @endauth
                  
                  @guest
                  <a href="/film/{{$item->id}}" class="btn btn-primary btn-sm">DETAIL</a>
                  @endguest
                </div>
              </div>
        </div>
    @empty
        <h1>Daftar Film Belum Terinput</h1>
    @endforelse
    
</div>
@endsection