<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('halaman.register');
    }
    
    public function submit(Request $request) {
         $nama1 = $request['FirstName'];
         $nama2 = $request['LastName'];
         return view('halaman.home', compact('nama1', 'nama2'));
    }
};

