<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use DB;
use App\film;
use File;

class FilmController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = film::all();
        return view('film.index', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = DB::table('genre')->get();
        return view('film.create', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg'
        ]);

        $pinposter = time().'.'.$request->poster->extension();  
   
        $request->poster->move(public_path('images'), $pinposter);
        
        $film = new film;
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->genre_id = $request->genre_id;
        $film->poster = $pinposter;

        $film->save();

        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = film::findOrfail($id);
        return view('film.show', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = DB::table('genre')->get();
        $film = film::findOrfail($id);
        return view('film.edit', compact('film', 'genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg'
        ]);

        if($request->has('poster')){
            $film = Film::find($id);

            $path = "images/";
            File::delete($path . $film->poster);

            $pinposter = time().'.'.$request->poster->extension();  
   
            $request->poster->move(public_path('images'), $pinposter);

            $film->judul = $request->judul;
            $film->ringkasan = $request->ringkasan;
            $film->tahun = $request->tahun;
            $film->genre_id = $request->genre_id;
            $film->poster = $pinposter;

            $film->save();

            return redirect('/film');
        }else{
            $film = Film::find($id);
            $film->judul = $request->judul;
            $film->ringkasan = $request->ringkasan;
            $film->tahun = $request->tahun;
            $film->genre_id = $request->genre_id;

            $film->save();

            return redirect('/film');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id);

            $path = "images/";
            File::delete($path . $film->poster);

            $film->delete();

            return redirect('/film');
    }
}
